FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	gcc \
	make \
	zlib1g-dev \
	libncurses5-dev

#Copy and extract SAMtools archive
ADD samtools-1.2.tar.bz2 /opt/

WORKDIR /opt/samtools-1.2

# Make SAMtools
RUN make

RUN make prefix=/opt/samtools-1.2 install
# Setting up SAMtools enviromental variable
ENV PATH /opt/samtools-1.2/bin:$PATH

#Set entrypoint on SAMtools
ENTRYPOINT ["samtools"]




	
